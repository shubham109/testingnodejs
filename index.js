const oracledb = require('oracledb');
const express = require('express');
const app = express();
oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;
oracledb.autoCommit = true;
app.get('/display',async(req,res)=>{
    let connection = null
    try{
        let connection = await oracledb.getConnection({
            user: "test_demo",
            password: "test_demo",
            connectString: "192.168.1.34:1521/orclweb"
        });
        const result = await connection.execute(
                        `SELECT * FROM employee_master`
                    );
                    console.log(result.rows)
                   return  res.json(result.rows)
    }
    catch(err){
        console.log(err)
        return res.send("Error")
    }
})
app.listen(3000)